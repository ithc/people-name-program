People Name Program

usage: java -jar peoplenameprogram.jar --input-file FILENAME --modified-name-limit NUMBER

Required Parameters:

--input-file            FILENAME    Specifies the path and name of the file containing the name data
--modified-name-limit   NUMBER      Specifies the limit for the modified names output, Default 25
--max-list-limit        NUMBER      Specifies the limit for the lists, Default 10
--loose                             Do not require the names in the input file to appear alphabetically
--help                              This message