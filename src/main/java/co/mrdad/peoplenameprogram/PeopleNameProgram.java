package co.mrdad.peoplenameprogram;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

public class PeopleNameProgram {
	public static void main(String args[]) {
		String inputFileName = "";
		int modifiedNameLimit = 25;
		int maxListLimit = 10;
		boolean loose = false;

		PeopleNameProgram pnp = new PeopleNameProgram();

		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("--input-file") && i < args.length - 1) {
				inputFileName = args[++i];
			} else if (args[i].equals("--modified-name-limit") && i < args.length - 1) {
				try {
					modifiedNameLimit = Integer.parseInt(args[++i]);
				} catch (NumberFormatException nfe) {
					pnp.printHelp();
				}
			} else if (args[i].equals("--max-list-limit") && i < args.length - 1) {
				try {
					maxListLimit = Integer.parseInt(args[++i]);
				} catch (NumberFormatException nfe) {
					pnp.printHelp();
				}
			} else if (args[i].equals("--help")) {
				pnp.printHelp();
				System.exit(0);
			} else if (args[i].equals("--loose")) {
				loose = true;
			}
		}

		if (inputFileName.isEmpty() || modifiedNameLimit < 1) {
			pnp.printHelp();
		} else {
			pnp.processData(inputFileName, loose, maxListLimit, modifiedNameLimit);
		}
	}

	public void processData(String fileName, boolean loose, int maxListLimit, int modifiedNameLimit) {
		Processor processor = new Processor();
		processor.load(fileName, loose);
		processor.printStats(maxListLimit, modifiedNameLimit);
	}

	public void printHelp() {
		StringBuilder output = new StringBuilder();
		InputStream in = getClass().getResourceAsStream("/help.txt");
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		String line;
		try {
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}

			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
