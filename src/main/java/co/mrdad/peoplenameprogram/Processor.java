package co.mrdad.peoplenameprogram;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Processor {
	private static String NAME_REGEX = "^([a-zA-Z]+), ([a-zA-Z]+)";

	private Set<String> fullNames = new HashSet<String>();
	private Map<String, Integer> firstNames = new HashMap<String, Integer>();
	private Map<String, Integer> lastNames = new HashMap<String, Integer>();
	private Map<String, String> modifiedNames = new HashMap<String, String>();


	public void load(String filename, boolean loose) {
		File inputFile = new File(filename);
		Pattern namePattern = Pattern.compile(NAME_REGEX);
		Matcher matcher;

		try {
			Scanner inputScanner = new Scanner(inputFile);
			String currentLine;
			String previousName = "";
			String firstName = "";
			String lastName = "";

			while (inputScanner.hasNextLine()) {
				currentLine = inputScanner.nextLine();
				matcher = namePattern.matcher(currentLine);
				if (matcher.find() && (loose || previousName.compareTo(matcher.group(1) + ", " + matcher.group(2)) < 0)) {
					firstName = matcher.group(2);
					lastName = matcher.group(1);
					
					previousName = String.format("%s, %s", lastName, firstName);

					if (!firstNames.containsKey(firstName) && !lastNames.containsKey(lastName)) {
						modifiedNames.put(lastName, firstName);
					}

					fullNames.add(String.format("%s, %s", lastName, firstName));
					if (firstNames.containsKey(firstName)) {
						firstNames.put(firstName, firstNames.get(firstName) + 1);
					} else {
						firstNames.put(firstName, 1);
					}

					if (lastNames.containsKey(lastName)) {
						lastNames.put(lastName, lastNames.get(lastName) + 1);
					} else {
						lastNames.put(lastName, 1);
					}
				}
			}
			inputScanner.close();
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		}
	}

	public void printStats(int sortedListLimit, int modifiedNameLimit) {
		System.out.println("1. The unique count of full, last, and first names.");
		System.out.println(String.format("%-20s%d", "Full Names:", fullNames.size()));
		System.out.println(String.format("%-20s%d", "First Names:", firstNames.keySet().size()));
		System.out.println(String.format("%-20s%d", "Last Names:", lastNames.keySet().size()));

		System.out.println();
		System.out.println("2. The top " + sortedListLimit + " most common last names");
		printSortedNames(lastNames, sortedListLimit);
		System.out.println();
		System.out.println("3. The top " + sortedListLimit + "  most common first names");
		printSortedNames(firstNames, sortedListLimit);
		System.out.println();
		System.out.println("4. A list of modified names");
		printModifiedNames(modifiedNameLimit);
	}

	private void printSortedNames(Map<String, Integer> inputMap, int max) {
		HashMap<String, Integer> sorted = sortMap(inputMap);
		Iterator<Map.Entry<String, Integer>> it = sorted.entrySet().iterator();
		int i = 0;
		System.out.println();
		System.out.println(String.format("%-30s%s", "Name", "Occurrences"));
		while (it.hasNext() && i < max) {
			Map.Entry<String, Integer> entry = it.next();
			System.out.println(String.format("%-30s%d", entry.getKey(), entry.getValue()));
			i++;
		}
	}

	private void printModifiedNames(int max) {
		List<String> lastNames = new ArrayList<String>();
		List<String> firstNames = new ArrayList<String>();
		Iterator<Map.Entry<String, String>> it = modifiedNames.entrySet().iterator();
		int i = 0;

		while (it.hasNext() && i < max) {
			Map.Entry<String, String> entry = it.next();
			lastNames.add(entry.getKey());
			firstNames.add(entry.getValue());
			i++;
		}
		max = i;
		for (i = 0; i < max; i++) {
			System.out.println(lastNames.get(i) + ", " + firstNames.get(max - i - 1));
		}

	}

	private static HashMap<String, Integer> sortMap(Map<String, Integer> hm) {
		List<Map.Entry<String, Integer> > list = new LinkedList<Map.Entry<String, Integer> >(hm.entrySet());

		Collections.sort(list, new Comparator<Map.Entry<String, Integer> >() {
			public int compare(Map.Entry<String, Integer> o1,
			                   Map.Entry<String, Integer> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});

		HashMap<String, Integer> temp = new LinkedHashMap<String, Integer>();
		for (Map.Entry<String, Integer> aa : list) {
			temp.put(aa.getKey(), aa.getValue());
		}
		return temp;
	}
}
