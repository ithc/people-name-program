# People Name Program

There are two versions of the output.  One is strict rules checking where the other one is not.

The reason for these two versions was that one of the rules required the names to be ordered alphabetically
in the input file.

The program will default to a strict adherence to the rules for processing the input file.
Utilizing the '--loose' parameter to the execution will allow the program to process all names
in the files as long as the name is at the beginning of the line.

Below is the help text from the program.  This can be rendered with the --help parameter.

People Name Program

usage: java -jar peoplenameprogram.jar --input-file FILENAME --modified-name-limit NUMBER

Required Parameters:

--input-file            FILENAME    Specifies the path and name of the file containing the name data
--modified-name-limit   NUMBER      Specifies the limit for the modified names output, Default 25
--max-list-limit        NUMBER      Specifies the limit for the lists, Default 10
--loose                             Do not require the names in the input file to appear alphabetically
--help                              This message